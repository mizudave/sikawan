import React, { Component } from 'react'
import axios from 'axios'
import { View } from 'react-native-animatable'
import { styles, post } from '../Style/Style'
import { Text, TouchableOpacity, SafeAreaView, Image, TextInput, AsyncStorage, ScrollView } from 'react-native'

export default class Show extends Component {

  constructor(props) {
    super(props)
  
    this.state = {
       keluhan : [],
    }
  }
  

  componentDidMount() {

    AsyncStorage.getItem('user', (error, result) => {

      if (result) {
        axios.get(
          'https://sikawankawan.herokuapp.com/student',
          // "http://192.168.100.10:8080/student",
          {headers: {
              "Authorization" : result
    
            }
          }
        )
        
          .then((result) => {
            console.log(result);
            
            this.setState({keluhan : result.data.data.complaint})
            
          })
        }
        });
  }

  render() {
    return (

      <View style = { styles.container }>
        
        <View style = { post.containerImage }>

          <ScrollView>
          <View style = { post.keluhan }>
          {this.state.keluhan.map(function(item,index){
            return (
              <View style={{borderWidth:1,padding:10,margin:10}} key={index}>
            <Text>{item.complaint}</Text>    

            <View style={{borderWidth:1,padding:10,margin:10}} key={index}>
            <Image
                  style={{width: 50, height: 50}}
                  source={{uri: `${item.image}`}} />
            </View>
            </View>

            )
          })}   

        </View>
        </ScrollView>


        <View style = { post.containerTambah }>

          <TouchableOpacity style = { post.button }>
            <Text style = {{ fontSize: 28, fontWeight: 'bold' }}> + </Text>
          </TouchableOpacity>

        </View>

      </View>
      </View>

    )
  }
}
