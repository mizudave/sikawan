import React, { Component } from 'react'
import axios from 'axios'
import { Picker, Text, View, TouchableOpacity,FlatList,ActivityIndicator, ScrollView } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons'

export default class Nilai extends Component {
  
  constructor(props){
    super(props);
    this.state = {
        nilai : [],
        loading : true,
        nama :'',
    }
}
componentDidMount() {

  AsyncStorage.getItem('user', (error, result) => {

    if (result) {
      axios.get(
        "https://sikawankawan.herokuapp.com/student",
        // "http://192.168.100.10:8080/student",
        {headers: {
            // "Authorization" : `${this.props.navigation.state.params.token}`
            "Authorization" : result
  
          }
        }
      )      
        .then((result) => {
          // console.log(result.data.data.fullname);
          this.setState({nilai : result.data.data.score.details,loading:false})
        })
      }
      });
}



  render() {
  const {loading}=this.state
    // console.log(this.props);

  if(!this.state.loading){
  return(
  <ScrollView>
  <View>
    <View style={{padding:20,backgroundColor: "white", borderRadius:5, }}>
      <View style={{
          flexDirection:"row", 
          justifyContent: "space-around",
          margin:5,
          }}>
          <TouchableOpacity style={{backgroundColor: "#4384F4",padding:20, alignItems:"center",borderRadius: 10,}} 
          onPress={()=>this.props.navigation.navigate('Details',
          {nilai:this.state.nilai.filter(item => {return item.category === "quiz"}).map(function(item){
          return {item}
          }),category:"Quiz"})}>
          <Icons name="folder-account-outline" size={50} color="black" header={null}/>
          <Text style={{fontSize: 20}}> QUIZ </Text>
          </TouchableOpacity>
          <TouchableOpacity style={{backgroundColor: "#4384F4",padding:20, alignItems:"center",borderRadius: 10,}}
          onPress={()=>this.props.navigation.navigate('Details',
          {nilai:this.state.nilai.filter(item => {return item.category === "assignment"}).map(function(item){
          return {item}
          }),category:"Tugas"})}>
          <Icons name="folder-account-outline" size={50} color="black" header={null}/>
          <Text style={{fontSize: 20}}> Tugas </Text>
          </TouchableOpacity>
        </View>
        <View style={{
          flexDirection:"row", 
          justifyContent: "space-around",
          margin:5,
          marginTop: 10,
          }}>
          <TouchableOpacity style={{backgroundColor: "#4384F4",padding:20, alignItems:"center",borderRadius: 10,}}
          onPress={()=>this.props.navigation.navigate('Details',
          {nilai:this.state.nilai.filter(item => {return item.category === "uts"}).map(function(item){
          return {item}
          }),category:"UTS"})}>
          <Icons name="folder-account-outline" size={50} color="black" header={null}/>
          <Text style={{fontSize: 20}}> UTS </Text>
          </TouchableOpacity>
          <TouchableOpacity style={{backgroundColor: "#4384F4",padding:20, alignItems:"center",borderRadius: 10,}}
          onPress={()=>this.props.navigation.navigate('Details',
          {nilai:this.state.nilai.filter(item => {return item.category === "uas"}).map(function(item){
          return {item}
          }),category:"UAS"})}>
          <Icons name="folder-account-outline" size={50} color="black" header={null}/>
          <Text style={{fontSize: 20}}> UAS </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
    </ScrollView>
  )
  } else {
    return (
      <View>
      <ActivityIndicator size="large" color="#4384F4"/>
      </View>
    )
  }
    
  }
}
