import React, { Component } from 'react'
import { styles, login } from '../Style/Style'
import axios from 'axios'
import { Text, View, TouchableOpacity, TextInput, ScrollView, Image } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';

export default class Login extends Component {

    constructor(props){
        super(props);
        this.state = {
            username : '',
            password : '',
            email : '',
            token : '',
        }
    }

    login = () => {
 
          const requestBody = {
            username : '000000000111111111',
            // username: this.state.username,
            // password: this.state.password,
            password : 'abcabc',
          }
          axios.post("https://sikawankawan.herokuapp.com/login",
        //   requestBody)

        //   axios.post("http://192.168.100.10:8080/login",
          requestBody)
          
          .then((result) => {
              let user = result.data.data.token
              AsyncStorage.setItem('user',user);
              this.props.navigation.navigate('Dashboard')
          })
        
        };


  render() {
      console.log("login");
      
    return (

    <View style = { styles.container }>

        <View style = { login.containerImage }>
            <Image style = { login.image } source = { require ( '../../img/logo.jpg' ) } />
        </View>

        <ScrollView>
        <View style = { login.containerForm }>

            <View style = { login.containerTeks }>
                <Text style = { login.teksHeader }>
                    SiKawan
                </Text>

                <Text style = { login.teksKeterangan }>
                    Silakan masukkan nomor NISN dan Password yang diberikan sekolah
                </Text>
            </View>

            <View style = { login.containerForm }>
                <View style = { login.form }>
                    <Text>
                        NISN : 
                    </Text>

                    <TextInput style = { login.nisn } placeholder = 'NISN'/>

                    <Text>
                        Password : 
                    </Text>

                    <TextInput style = { login.password } placeholder = 'Password'/>

                </View>
            </View>
        </View>
        </ScrollView>

        <TouchableOpacity style = { login.button } onPress={()=>this.login()}>
            <Text style = {{ fontSize: 24, color: 'white' }}> 
                Masuk 
            </Text>
        </TouchableOpacity>

    </View>

    )
  }
}
