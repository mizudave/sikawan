import React, { Component } from 'react'
import { Image, StyleSheet, Dimensions, SafeAreaView, ScrollView, ActivityIndicator } from 'react-native'
import { createAnimatableComponent, View, Text } from 'react-native-animatable';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios'
import AnimatedBar from "../Nilai/AnimatedBar";

export default class Home extends Component {

constructor(props){
    super(props);
    this.state = {
        nilai : [],
        loading : true,
        nama: "",
        kelas:"",
        address:"",
    }

}
  componentDidMount() {

    AsyncStorage.getItem('user', (error, result) => {

      if (result) {
        axios.get(
          "https://sikawankawan.herokuapp.com/student",
          // "http://192.168.100.10:8080/student",
          {headers: {
              "Authorization" : result
    
            }
          }
        )
        
          .then((result) => {
            // console.log(result.data.data.fullname);
            this.setState({
                nilai : result.data.data.score.details,
                nama : result.data.data.fullname,
                kelas : result.data.data.kelas.fullname,
                address : result.data.data.address,
                loading: false,
            })
          })
        }
        });
  }

  render() {
    if(!this.state.loading){
    return (
    <View style={{flex:1,}}>
      <View style={{
          borderWidth:1,
          height:"25%",
          backgroundColor:"#4384F4"
          }}>
          <View style={{
              alignItems:"center",
              flex:1,
          }}>
                <Image source={require('../../img/a.jpg')} style={{
                    borderRadius:50,
                    borderColor:"white",
                    borderWidth:1,
                    width:100,
                    height:100,
                    marginTop:5,
                }}/>
          </View>
          <View style={{
              flexDirection:"row",
              justifyContent:"space-around",
              alignItems:"flex-end",
              flex:1,
          }}>
              <Text style={{fontSize:16,fontWeight:"bold"}}> Nama</Text>
              <Text style={{fontSize:16,fontWeight:"bold"}}> Kelas</Text>
          </View>
          <View style={{
              flexDirection:"row",
              justifyContent:"space-around",
              alignItems:"flex-start",
              flex:1,
          }}>
              <Text style={{fontSize:16,fontWeight:"bold"}}> {this.state.nama} </Text>
              <Text style={{fontSize:16,fontWeight:"bold"}}> {this.state.kelas} </Text>
          </View>
          <View style={{
              alignItems:"center",
              margin:5,
              borderColor:"black",
              borderWidth:1, 
              backgroundColor:"white",
              padding:5,}}>
              <Text>{this.state.address}</Text>
          </View>
      </View>
      <View style={{
          borderWidth:1,
          margin:10,
          borderRadius:5,
          height:"20%",
      }}>
        <Text style={{alignSelf:"center",fontSize:20,fontWeight:"bold"}}>Kehadiran</Text>
        <View style={{
            flexDirection:"row",
            justifyContent: "space-around",
            alignItems:"center",
            flex:1,
        }}>
            <View style={{
                borderRadius:50,
                borderWidth:1,
                height:80,
                width:80,
                borderColor:"#009fff",
                backgroundColor: "#009fff",
                justifyContent:"center",
                alignItems:"center",
            }}>
                <Text style={{
                    color:"white",
                    fontWeight:"bold",
                    fontSize:20,
                }}>0</Text>  
                <Text style={{
                    color:"white",
                    fontWeight:"bold",
                    fontSize:16,
                }}>Hadir</Text>  
            </View>
            {/* <View style={{
                borderRadius:50,
                borderWidth:1,
                height:80,
                width:80,
                borderColor:"#ff9d00",
                backgroundColor: "#ff9d00",
                justifyContent:"center",
                alignItems:"center",
            }}>
                <Text style={{
                    color:"white",
                    fontWeight:"bold",
                    fontSize:20,
                }}>0</Text>  
                <Text style={{
                    color:"white",
                    fontWeight:"bold",
                    fontSize:15,
                }}>Tidak Penuh</Text>  
            </View> */}
            <View style={{
                borderRadius:50,
                borderWidth:1,
                height:80,
                width:80,
                borderColor:"#27a003",
                backgroundColor: "#27a003",
                justifyContent:"center",
                alignItems:"center",
            }}>
                <Text style={{
                    color:"white",
                    fontWeight:"bold",
                    fontSize:20,
                }}>0</Text>  
                <Text style={{
                    color:"white",
                    fontWeight:"bold",
                    fontSize:16,
                }}>Sakit</Text>  
            </View>
            <View style={{
                borderRadius:50,
                borderWidth:1,
                height:80,
                width:80,
                borderColor:"#d30000",
                backgroundColor: "#d30000",
                justifyContent:"center",
                alignItems:"center",
            }}>
                <Text style={{
                    color:"white",
                    fontWeight:"bold",
                    fontSize:20,
                }}>0</Text>  
                <Text style={{
                    color:"white",
                    fontWeight:"bold",
                    fontSize:14,
                }}>Alpa</Text>  
            </View>
        </View>
      </View>
      <View style={{
          borderWidth:1,
          flex:1,
          backgroundColor:"#4384F4",
        //   height:"30%",
        //   margin:10,
        //   borderRadius:10,
      }}>
          <Text style={{alignSelf:"center",fontSize:20,fontWeight:"bold"}}>Nilai</Text>
          <ScrollView style={{flex:1,}}>
          {this.state.nilai.sort(function(a,b){
              let dateA=new Date(a.createdAt),dateB=new Date(b.createdAt)
              return dateB-dateA;
          }).slice(0,5).map(function(item,index){
            return(
                // <View >
                <View style={{margin:10, borderWidth:1, padding:10,backgroundColor:"white",borderRadius:5}} key={index}>
                  <View>
                    <View>
                      <Text>Mata Pelajaran: {item.subjects.name.toUpperCase()}</Text>
                    </View>
                  </View>
                  <View>
                    <View>
                      <Text>Tanggal: {item.createdAt.slice(0,10)}</Text>
                    </View>
                  </View>
                  <View>
                    <View>
                      <Text>Nilai: {item.point}</Text>
                      {/* <View style={{borderWidth:1, borderRadius:5,}}> */}
                        <View style={{borderWidth:1,margin:5}}>
                          <AnimatedBar value={item.point} index={20} />
                        {/* </View> */}
                      </View>
                    </View>
                  </View>
                </View>  
                // </View>
                )}
              // }
              )
      }
      </ScrollView>
      </View>
    </View>
    )} else {
      return(
      <View>
        <ActivityIndicator size="large" color="#4384F4"/>
      </View>
      )
    }
  }
}
