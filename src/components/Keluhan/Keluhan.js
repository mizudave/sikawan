import React, { Component } from 'react'
import { View } from 'react-native-animatable'
import { styles, keluhan } from '../Style/Style'
import { Text, TextInput, TouchableOpacity, SafeAreaView } from 'react-native'

export default class Keluhan extends Component {
  render() {
    return (

      <View style = { styles.container }>
        
        <View style = { keluhan.form }>

          
          <View style = { keluhan.form1 }>
            <View animation="bounceIn" useNativeDriver duration={800}>
              <SafeAreaView>
                <Text style = { keluhan.teks }> 
                    Keluhan anda akan sampai ke dinas dan pihak sekolah 
                </Text>
              </SafeAreaView>
            </View>
          </View>
          
          <View style = { keluhan.form2 }>
            <View animation="bounceIn" useNativeDriver duration={800}>
              <SafeAreaView>
                <View>
                  <View style = { keluhan.kotakImage }>
                      {/* <Image /> */}
                      <Text style = {{ fontSize: 42, fontWeight: 'bold' }}> + </Text>
                  </View>
                </View>
              </SafeAreaView>
            </View>
          </View>
            
          
          <View style = { keluhan.form3 }>
            <View animation="bounceIn" useNativeDriver duration={800}>
              <SafeAreaView>
                <Text style = { keluhan.teks }> Tulis keluhan </Text>
                <TextInput style = { keluhan.inputTeks } placeholder = 'Komplain Keluahn'/>
              </SafeAreaView>
            </View>
          </View>

        </View>

        <TouchableOpacity style = { keluhan.button }>
            <Text style = {{ fontSize: 24, color: 'white' }}> 
                Lapor Keluhan
            </Text>
        </TouchableOpacity>

      </View>
    )
  }
}
